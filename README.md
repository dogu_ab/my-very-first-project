# My very first project

## Install
Load modules first
* **npm i**

Start server.js
* **npm start**

## Usage
Open [http://localhost:3000](http://localhost:3000)

## Filtering
Open [http://localhost:3000/api/stores](http://localhost:3000/api/stores) to see full list of stores.

### Filtering Options
You can try to add **nsfw** and **location** parameters.

Thank You.

